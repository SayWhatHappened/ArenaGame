package me.emils.arenagame.arena;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.emils.arenagame.ArenaGame;
import me.emils.arenagame.managers.ChatManager;
import net.md_5.bungee.api.ChatColor;

public class ArenaManager {

	private ArenaConfig arenaConfig;
	private ArenaGame game;

	public ArenaManager(ArenaConfig arenaConfig, ArenaGame game) {
		this.arenaConfig = arenaConfig;
		this.game = game;
	}

	public void setLobby(Location location, Player player) {
		this.arenaConfig.getConfig().set("lobbyLocation", location);
		this.arenaConfig.save();
		player.sendMessage(ChatColor.translateAlternateColorCodes('&',
				game.getChatManager().getGlobalPrefix() + "&7Lobby location set!"));
	}

	public void createArena(String arg, Player player) {

		if (!(Bukkit.getWorld(arg) == null)) {
			player.sendMessage(
					ChatColor.translateAlternateColorCodes('&', game.getChatManager().getGlobalPrefix() + "&7Loading world..."));
			Location loc = new Location(Bukkit.getWorld(arg), 64, 65, 2);
			player.teleport(loc);
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', game.getChatManager().getGlobalPrefix()
					+ "&7You were teleported to the default spawn location of " + arg + "."));

			this.arenaConfig.getConfig().createSection(arg);
			this.arenaConfig.save();

			player.sendMessage(" ");
			player.sendMessage(ChatColor.translateAlternateColorCodes('&',
					game.getChatManager().getGlobalPrefix() + "&cYou can now start configuring the arena."));
		} else {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&',
					game.getChatManager().getGlobalPrefix() + "&cWorld does not exist."));
		}
	}

	public void teleport(String arg, Player player) {

		if (!(Bukkit.getWorld(arg) == null)) {
			Location loc = new Location(Bukkit.getWorld(arg), 64, 65, 2);
			player.teleport(loc);
			player.sendMessage(ChatColor.translateAlternateColorCodes('&',
					game.getChatManager().getGlobalPrefix() + "&7You were teleported to " + arg + "."));
		} else {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&',
					game.getChatManager().getGlobalPrefix() + "&cWorld does not exist."));
		}
	}

	public void setSpectator(Location location, Player player) {
		if (player.getWorld().getName()
				.equals(String.valueOf(this.arenaConfig.getConfig().getStringList("lobbyLocation.world")))) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&',
					game.getChatManager().getGlobalPrefix() + "&cCan't set spectator spawn in lobby!"));
		} else {
			this.arenaConfig.getConfig().getConfigurationSection(player.getWorld().getName()).set("spectatorLocation",
					location);
			this.arenaConfig.save();
			player.sendMessage(ChatColor.translateAlternateColorCodes('&',
					game.getChatManager().getGlobalPrefix() + "&7Spectator location set!"));
		}
	}

	public void setSpawn(Player p, int index) {
		this.arenaConfig.getConfigurationSection(p.getWorld().getName()).set("playerLocation.location" + index, p.getLocation());
		this.arenaConfig.save();

		p.sendMessage(
				ChatColor.translateAlternateColorCodes('&', game.getChatManager().getGlobalPrefix() + "&7Player location set!"));
	}
}
