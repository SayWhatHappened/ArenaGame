package me.emils.arenagame.arena;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ArenaConfig {

    private static File file;
    private static FileConfiguration customFile;

    public static void setup(){
        file = new File(Bukkit.getServer().getPluginManager().getPlugin("ArenaGame").getDataFolder(), "arena.yml");

        if (!file.exists()){
            try{
                file.createNewFile();
            }catch (IOException e){
                return;
            }
        }
        customFile = YamlConfiguration.loadConfiguration(file);
    }

    public FileConfiguration getConfig(){
        return customFile;
    }

    public void save(){
        try{
            customFile.save(file);
        }catch (IOException e){
            System.out.println("Unable to save file");
        }
    }

    public void reload(){
        customFile = YamlConfiguration.loadConfiguration(file);
    }
}
