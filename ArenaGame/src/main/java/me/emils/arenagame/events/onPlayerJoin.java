package me.emils.arenagame.events;

import me.emils.arenagame.ArenaGame;
import me.emils.arenagame.managers.EventManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class onPlayerJoin extends EventManager {

    public onPlayerJoin(ArenaGame pl) {
        super(pl);
    }

    @EventHandler
    public void PlayerJoin(PlayerJoinEvent event) {
        if (!(event.getPlayer().hasPlayedBefore())) {
            return;
        }
        event.setJoinMessage(null);
    }
}
