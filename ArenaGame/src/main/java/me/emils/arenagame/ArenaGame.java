package me.emils.arenagame;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.emils.arenagame.arena.ArenaConfig;
import me.emils.arenagame.arena.ArenaManager;
import me.emils.arenagame.commands.ArenaGameCommand;
import me.emils.arenagame.events.onPlayerJoin;
import me.emils.arenagame.managers.ChatManager;
import me.emils.arenagame.managers.StateManager;

public final class ArenaGame extends JavaPlugin implements Listener {

	private ArenaConfig arenaConfig;
	private ArenaManager arenaManager;
	private ChatManager chatManager;
	
	/*
	 * Changes I made:
	 * Passed Config and Manager via constructor
	 * Renamed ArenaGame (command) into ArenaGameCommand - VERY bad practice having 2 classes
	 * with the same name in one project.
	 * 
	 * Good luck gamer!
	 * ~ Alex
	 * 
	 * PS! Stop using static.
	 * PPS! Like- for real. Stop.
	 * 
	 */
	
    @Override
    public void onEnable() {
        // Plugin startup logic
        this.saveDefaultConfig();
        ArenaConfig.setup();
        registerListeners();
        StateManager.setState(StateManager.IN_LOBBY);
        
        this.chatManager = new ChatManager();
        this.arenaConfig = new ArenaConfig();
        this.arenaManager = new ArenaManager(this.arenaConfig, this);
        
        this.getCommand("arenagame").setExecutor(new ArenaGameCommand(arenaManager));
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private static ArenaGame instance;

    public ArenaGame() {

        instance = this;
    }

    public static ArenaGame getInstance() {

        return instance;
    }

    public ChatManager getChatManager() {
    	return this.chatManager;
    }
    
    public void registerListeners() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(this, this);
        pm.registerEvents(new onPlayerJoin(this), this);
    }
}
