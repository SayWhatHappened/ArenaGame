package me.emils.arenagame.commands;

import me.emils.arenagame.arena.ArenaManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ArenaGameCommand implements CommandExecutor {
	
	private ArenaManager arenaManager;
	public ArenaGameCommand(ArenaManager arenaManager) {
		this.arenaManager = arenaManager;
	}
	
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (label.equalsIgnoreCase("arenagame")) {
                if (args.length < 1 || args[0].equalsIgnoreCase("help")) {
                    //TODO HELP MENU
                    return false;
                } else {
                    if (args[0].equalsIgnoreCase("arenalist")) {
                        //TODO ADD ARENA LIST
                        return false;
                    }
                    if (args[0].equalsIgnoreCase("setlobby")) {
                    	arenaManager.setLobby(player.getLocation(), player);
                    }
                    if (args[0].equalsIgnoreCase("setuparena")) {
                    	arenaManager.createArena(args[1], player);
                    }
                    if (args[0].equalsIgnoreCase("teleport")) {
                        arenaManager.teleport(args[1], player);
                    }
                    if (args[0].equalsIgnoreCase("setspectatorspawn")) {
                        arenaManager.setSpectator(player.getLocation(), player);
                    }
                    if (args[0].equalsIgnoreCase("setplayerspawn")) {
                        ArenaManager.setSpawnLocation(player, ArenaConfig.get().getConfigurationSection(player.getWorld().getName()).getKeys(false). size() + 1);
                    }
                }
            }
        }
        return false;
    }
}
