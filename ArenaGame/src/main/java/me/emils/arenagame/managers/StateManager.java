package me.emils.arenagame.managers;

public enum StateManager {

    IN_LOBBY(true), IN_GAME(false), RESETTING(false);

    private boolean canJoin;

    private static StateManager currentState;

    StateManager(boolean canJoin) {
        this.canJoin = canJoin;
    }

    public boolean canJoin() {
        return canJoin;
    }

    public static void setState(StateManager state) {
        StateManager.currentState = state;
    }

    public static boolean isState(StateManager state) {
        return StateManager.currentState == state;
    }

    public static StateManager getState() {
        return currentState;
    }
}
