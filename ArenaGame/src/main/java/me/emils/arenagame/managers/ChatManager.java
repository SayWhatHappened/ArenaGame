package me.emils.arenagame.managers;

import me.emils.arenagame.ArenaGame;

public class ChatManager {

    public String getGlobalPrefix () {
        return "&8&l❙ " + ArenaGame.getInstance().getConfig().getString("Prefix") + " &8❘ ";
    }
}
